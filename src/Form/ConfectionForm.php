<?php

namespace Drupal\confection\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfectionForm.
 */
class ConfectionForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'confection.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('confection.settings');

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#description' => $this->t('Your account ID can be found found in your Confection account dashboard.       <div class="confection_wrapper"> 
      
      <div class="inline-btn">
        <a href="https://dashboard.confection.io/" target="_blank" class="btn-purple">
          <span class="oi oi-account-login" data-glyph="account-login" title="account-login" aria-hidden="true"></span> 
          Log In to Your Confection Account
        </a>
      </div>
        <a href="https://dashboard.confection.io/register/" target="_blank" class="btn-green">
          <span class="oi " data-glyph="plus" title="plus" aria-hidden="true"></span>
          Need an Account? Set One Up. (Its Free.)
        </a>
    
    </div>'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('account_id'),
    ];
    $form['acontainer'] = [
      '#type' => 'container',
      '#attributes' =>
          [
            'class' => 'acontainer',
          ],
    ];

    $form['analytics'] = [
      '#type' => 'checkbox',
      '#weight' => 2,
      '#title' => $this->t('Confection Analytics'),
      '#title_display' => 'before',
      '#default_value' => $config->get('analytics'),
      '#description' => $this->t('Check this to disable Confection Analytics system, which will track common analytics data like pageviews and present them on our Dashboard.<br> If you preffer to disable it, just check this box. <strong>Default: Checked</strong>.'),
    ];
    $form['privacy_approach'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Privacy Approach'),
      '#description' => $this->t('Confection allows users to collect, store, and distribute data in accordance with the data protection law of their choice. Make your selection below.'),
      '#options' => [
        'none' => $this->t('None'),
        'gdpr' => $this->t('GDPR'),
        'ccpa' => $this->t('CCPA'),
        'lgpd' => $this->t('LGPD'),
      ],
      '#size' => 0,
      '#default_value' => $config->get('privacy_approach'),
    ];
    
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Privacy Banner Position'),
      '#description' => $this->t('Confection uses a compact, minimally-invasive banner to get user consent. Unlike other consent banners, ours only appears when necessary. Select the position of your banner.'),
      '#options' => [
        'none' => $this->t('None'),
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right')
      ],
      '#size' => 0,
      '#default_value' => $config->get('position'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('confection.settings')
      ->set('account_id', $form_state->getValue('account_id'))
      ->set('privacy_approach', $form_state->getValue('privacy_approach'))
      ->set('position', $form_state->getValue('position'))
      ->set('analytics', $form_state->getValue('analytics'))
      ->save();
  }
}
