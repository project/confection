<?php

namespace Drupal\confection\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
/**
 * Provides a 'ConfectionBlock' block.
 *
 * @Block(
 *  id = "confection_block",
 *  admin_label = @Translation("Confection"),
 * )
 */

class ConfectionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Defines the configuration object factory.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactory $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
  }
  
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->config->get('confection.settings');

    //Translate strings.
    $confection_strings = [
      'banner_none' => (string) $this->t('This site isn’t collecting your personal information. Any information you submitted before opting out is still in our system. To manage this information, please'),
      'banner_base' => (string) $this->t('The authors of this site care about your personal data. That’s why they use Confection. Our privacy-first data management app helps people like you take control of the information you share online.'),
      'banner_strict_base' => (string) $this->t('At the moment, this site would like permission to use basic data to improve the content it offers you. This would include information like your IP address. We won’t collect more sensitive information such as your name or email address without asking you first.'),
      'banner_collecting' => (string) $this->t('You’ve given this site permission to collect information like your IP address, name, and email.'),
      'banner_collecting_basic' => (string) $this->t('Collecting Basic Data'),
      'banner_collecting_full' => (string) $this->t('Fully Authorized'),
      'banner_collecting_not' => (string) $this->t('Not Collecting Your Data'),
      'banner_strict' => (string) $this->t('Hi, it’s Confection again. We noticed that you’re about to share information like your name and email with this site. Do we have your permission to do so?'),
      'button_more' => (string) $this->t('Learn More'),
      'button_accept' => (string) $this->t('Accept'),
      'button_deny' => (string) $this->t('Not now'),
      'button_stop' => (string) $this->t('Stop Collecting'),
      'button_resume' => (string) $this->t('Resume Data Sharing'),
      'button_close' => (string) $this->t('Close'),
      'button_click' => (string) $this->t('click here'),
    ];

    $build = [];
    $build['#theme'] = 'confection_block';

    $items = array();
    $items['account_id'] = $config->get('account_id');
    $items['privacy_approach'] = $config->get('privacy_approach');
    $items['position'] = $config->get('position');
    $items['confection_url'] = $config->get('confection_url');
    $items['analytics'] = $config->get('analytics');
    $items['tstring'] = $confection_strings;
    $build['#content'] = $items;

    return $build;
  }
}
