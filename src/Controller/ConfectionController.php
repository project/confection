<?php

namespace Drupal\confection\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


/**
 * Class ConfectionController.
 */
class ConfectionController extends ControllerBase {

  /**
   * {@inheritdoc}
   */



  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    return $instance;
  }
  
  public function cable(Request $request) {
    $http_client = new Client();

    $account_id = 0;

    if ($request->request->get('account_id')) {
      $account_id = $request->request->get('account_id');
    }
    $listener_url = 'https://substation.confection.io/';

    if (!empty($request->getClientIp())) {
      $ip = $request->getClientIp();
    } 
    else {
      $ip = "";
    }

    if ($request->request->get('event')) {
      $url = $listener_url . '?account_id=' . $account_id . '&uuid=' . $request->request->get('uuid') . '&event=' . urlencode($request->request->get('event')) . '&value=' . urlencode($request->request->get('value')) . '&ip=' . $ip . '&browser=' . urlencode($request->server->get("HTTP_USER_AGENT")) . '&domain=' . urlencode($request->request->get('domain'));
    }
    else {
      $url = $listener_url . '?account_id=' . $account_id . '&uuid=' . $request->request->get('uuid') . '&name=' . urlencode($request->request->get('name')) . '&value=' . urlencode($request->request->get('value')) . '&ip=' . $ip . '&browser=' . urlencode($request->server->get("HTTP_USER_AGENT")) . '&domain=' . urlencode($request->request->get('domain'));
    }
    try {
      $http_client->get($url);
    } catch (RequestException $e) {}

    return [
      '#markup' => $url
      
    ];
  }

}
