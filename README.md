# Confection contributed module for Drupal 8.x || 9

INTRODUCTION
------------

[Confection](https://confection.io/) collects, stores, and distributes data in a way that's unaffected by client-side disruptions involving cookies, cross-domain scripts, and device IDs. It's also compliant with global privacy laws so it’s good for people too. And it integrates with the apps businesses and developers already use. There’s no need to switch systems. Just plug in, power up, and keep your marketing partnerships running strong. 

Use this package to add Confection to your site. Need an account? Set one up @ [dashboard.confection.io/register](https://dashboard.confection.io/register) (It's free.)

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install like any other Drupal Module or use composer.
```
composer require drupal/confection

```

CONFIGURATION
-------------

After installation open confection config settings in Drupal and set the corresponding values found in your https://confection.io/ dashboard.  


```
 http://your_drupal_site/admin/config/confection
```



Place the newly installed Confection Block in the footer region of your Drupal theme. 
```
 http://your_drupal_site/admin/structure/block

```
