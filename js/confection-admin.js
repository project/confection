(function ($) {
  'use strict';
  Drupal.behaviors.confection = {
    attach: function (context, settings) {
      $('main', context).once('confection').each(function () {

        $('#edit-privacy-approach').change( function(){
          var msg_a = '',
            msg_b = '';

          $('.form-item-position').show();

          switch($('#edit-privacy-approach').val()) {
            case 'none':
              $('.form-item-position').hide();
              msg_a = 'I don’t want Confection to get a user’s consent to collect his/her data.';
              msg_b = 'Confection will collect all the data it can without waiting for user permission.';
              break;
            case 'gdpr':
              msg_a = 'I want Confection to collect, store, and distribute user data in accordance with the European Union’s General Data Protection Regulation.';
              msg_b = 'Until Confection gets consent from a user, it will only collect non-personally-identifying information.';
              break;
            case 'ccpa':
              msg_a = 'I want Confection to collect, store, and distribute user data in accordance with the California Consumer Privacy Act.';
              msg_b = 'Until Confection gets consent from a user, it will only collect non-personally-identifying information.';
              break;
            case 'lgpd':
              msg_a = 'I want Confection to collect, store, and distribute user data in accordance with Brazil’s Lei Geral de Proteção de Dados Pessoais.';
              msg_b = 'Until Confection gets consent from a user, it will not collect any information, personally identifying or otherwise. Confection will get a second consent from the user when it begins collecting personally identifying information.';
              break;
          }

          $('#edit-privacy-approach + span').remove();
          $('#edit-privacy-approach').after('<span><strong>'+ msg_a +'</strong><em>' + msg_b +'</em></span>');
        });
        $('#edit-privacy-approach').change();


        $('#edit-position').change( function(){
          var msg_a = '',
            msg_b = '';

          switch($('#edit-position').val()) {
            case 'none':
              msg_a = 'I will use other means to collect user consent.';
              msg_b = 'The Confection banner will not appear. Use the JavaScript function <code>confection.setConsent(2)</code> to start collecting data.';
              break;
            case 'left':
              msg_a = 'I want the banner to appear in the bottom left corner of the browser';
              msg_b = '';
              break;
            case 'right':
              msg_a = 'I want the banner to appear in the bottom right corner of the browser';
              msg_b = '';
              break;
            case 'center':
              msg_a = 'I want the banner to appear in at the bottom of the browser, in the middle of the screen.';
              msg_b = '';
              break;
          }

          $('#edit-position + span').remove();
          $('#edit-position').after('<span><strong>'+ msg_a +'</strong><em>' + msg_b +'</em></span>');
        });
        $('#edit-position').change();

      });
    }
  };
}(jQuery));
